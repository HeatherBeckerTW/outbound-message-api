﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OutboundMessageApi.Models;
using OutboundMessageApi.Core;
using Newtonsoft.Json;


namespace OutboundMessageApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Messages")]
    public class MessagesController : Controller
    {
        private const string CONTENT_TYPE_JSON = "application/json";

        private readonly ITelephony _telephony;
        private readonly ILogger<MessagesController> _logger;

        #region //// Constructors ////

        public MessagesController(ITelephony telephony, ILogger<MessagesController> logger)
        {
            _telephony = telephony;
            _logger = logger;
        }

        #endregion

        #region ////GET Methods ////

        [HttpGet]
        public IEnumerable<SmsMessage> Get()
        {
            return GetAllMessages();
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public SmsMessage Get(int id)
        //{
        //    SmsMessage message = new SmsMessage();
            
        //    //TODO: retrieve the message with specified id & return it

        //    return message;
        //}

        #endregion

        #region //// POST Methods ////

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]SmsMessage messageRequest)
        {
            _logger.LogInformation(LoggingEvents.SEND_MESSAGE_RECEIVED, "SMS request received");

            if (messageRequest == null)
            {                
                return Json(NullRequestInput());
            }

            _logger.LogTrace(LoggingEvents.SEND_MESSAGE_RECEIVED, JsonConvert.SerializeObject(messageRequest));
            return Json(HandleMessagePost(messageRequest));
        }

        #endregion

        #region //// Private Methods ////

        private IResponse HandleMessagePost(IMessage message)
        {
            //try to send the message
            return _telephony.Send(message);
        }

        private IResponse NullRequestInput()
        {
            _logger.LogWarning(LoggingEvents.REQUEST_MESSAGE_NULL, "Null input SMS object.");
            var resp = new SmsMessageResponse()
            {
                ErrorCode = LoggingEvents.REQUEST_MESSAGE_NULL.ToString(),
                ErrorMessage = "The input message was null.",
                IsSuccess = false
            };

            return resp;
        }

        private List<SmsMessage> GetAllMessages()
        {
            List<SmsMessage> messages = new List<SmsMessage>();
            _logger.LogInformation(LoggingEvents.GET_ALL_MESSAGES, $"Getting all messages. {messages.Count} results returned.");

            return messages;
        }

        #endregion
    }
}