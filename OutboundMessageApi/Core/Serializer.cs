﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OutboundMessageApi.Core
{
    public static class Serializer
    {
        public static string SerializeToJsonString(Object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
