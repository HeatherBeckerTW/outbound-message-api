﻿
namespace OutboundMessageApi.Core
{
    public static class LoggingEvents
    {
        public const int SERVICE_STARTING = 999;

        public const int GET_ALL_MESSAGES = 1000;
        public const int GET_MESSAGE = 1001;
        public const int SEND_MESSAGE_RECEIVED = 1002;
        public const int MESSAGE_SENT = 1003;

        public const int REQUEST_MESSAGE_NULL = 4000;

        public const int MESSAGE_NOT_FOUND = 5000;
        public const int MESSAGE_NOT_SENT = 5001;
        public const int TWILIO_CREDS_NOT_FOUND = 5002;
        public const int CONFIG_SETTING_NOT_FOUND = 5003;

        public const int SHUTDOWN_ERROR = 9999;
    }
}
