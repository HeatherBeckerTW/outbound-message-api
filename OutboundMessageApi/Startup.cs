﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OutboundMessageApi.Models;
using NLog.Web;
using NLog.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using System.Web.Http;
using System.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace OutboundMessageApi
{
    public partial class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //set up the twilio class for dependency injection
            services.AddScoped<ITelephony, TwilioTelephony>();

            //ignore any non-https requests
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) 
        {
            //remove the Server header which leaks architecture details
            app.Use((context, next) =>
            {
                context.Response.Headers.Add("Server", string.Empty);
                return next();
            });

            //if it's the dev environment, show the exceptions page for debugging
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //configure nlog for text logging
            env.ConfigureNLog("nlog.config");
            loggerFactory.AddNLog();

            //force the user of Https
            var options = new RewriteOptions().AddRedirectToHttps();
            app.UseRewriter(options);

            //enable the mvc framework
            app.UseMvc();
            
        }
    }
}
