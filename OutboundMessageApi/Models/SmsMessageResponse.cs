﻿using System.Net;
using System.Runtime.Serialization;

namespace OutboundMessageApi.Models
{
    [DataContract]
    public class SmsMessageResponse : IResponse
    {
        //outputs
        [DataMember]
        public bool IsSuccess { get; set; }

        [DataMember]
        public string ErrorCode { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }
}
