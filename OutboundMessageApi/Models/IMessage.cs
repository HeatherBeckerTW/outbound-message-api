﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutboundMessageApi.Models
{
    public interface IMessage
    {
        string ClientId { get; set; }
        string NumberTo { get; set; }
        string NumberFrom { get; set; }
        string MessageBody { get; set; }
    }
}
