﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutboundMessageApi.Models
{
    public interface IResponse
    {
        bool IsSuccess { get; set; }
        string ErrorCode { get; set; }
        string ErrorMessage { get; set; }
    }
}
