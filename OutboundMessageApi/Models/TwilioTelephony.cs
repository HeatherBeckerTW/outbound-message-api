﻿using System;
using Twilio.Rest.Api.V2010.Account;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.Logging;
using OutboundMessageApi.Core;
using System.Net;

namespace OutboundMessageApi.Models
{
    public class TwilioTelephony : ITelephony
    {
        #region //// Constants ////

        private const string USERNAME_KEY = "TWILIO_ACCOUNT_ID";
        private const string PASSWORD_KEY = "TWILIO_AUTH_TOKEN";

        #endregion

        #region //// Fields ////

        private static IConfiguration _configuration { get; set; }
        private readonly ILogger<TwilioTelephony> _logger;

        #endregion

        #region //// Constructors ////

        public TwilioTelephony(ILogger<TwilioTelephony> logger)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            _configuration = builder.Build();
            _logger = logger;
        }

        #endregion


        public IResponse Send(IMessage message)
        {
            SmsMessageResponse response = new SmsMessageResponse();
            string clientAccountId = string.Empty;
            string clientAuthToken = string.Empty;
            string fromNumber = message.NumberFrom;

            try
            {
                clientAccountId = _configuration[$"{message.ClientId}:{USERNAME_KEY}"];
                clientAuthToken = _configuration[$"{message.ClientId}:{PASSWORD_KEY}"];
                string tempFrom = _configuration[$"{message.ClientId}:Override_from"];
                if (!string.IsNullOrEmpty(tempFrom))
                    fromNumber = tempFrom;
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.TWILIO_CREDS_NOT_FOUND, ex, ex.Message);

                response = CreateSmsResponse($"TW-{LoggingEvents.TWILIO_CREDS_NOT_FOUND}", "An unexpected error occurred when attempting to send SMS. Please review logs for details.");
            }

            try
            {
                //send the message
                Twilio.TwilioClient.Init(clientAccountId, clientAuthToken);
                var messageResource = MessageResource.Create(
                   from: new Twilio.Types.PhoneNumber(fromNumber),
                   to: new Twilio.Types.PhoneNumber(message.NumberTo),
                   body: message.MessageBody);

                response = CreateSmsResponse(
                        messageResource.ErrorCode.HasValue ? messageResource.ErrorCode.ToString() : null,
                        string.IsNullOrEmpty(messageResource.ErrorMessage) ? null : messageResource.ErrorMessage,
                        messageResource.ErrorCode.HasValue == false
                    );
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.MESSAGE_NOT_SENT, ex, ex.Message);
                
                response = CreateSmsResponse($"TW-{LoggingEvents.MESSAGE_NOT_SENT}", 
                    "An unexpected error occurred when attempting to send SMS. Please retry, and if the issue continues, contact Customer Support.");
            }                    

            return response;
        }

        private SmsMessageResponse CreateSmsResponse(string errorCode, string errorMessage, bool isSuccess = false)
        {
            return new SmsMessageResponse()
            {
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                IsSuccess = isSuccess
            };
        }
    }
}
