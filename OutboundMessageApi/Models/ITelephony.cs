﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OutboundMessageApi.Models
{
    public interface ITelephony
    {
        IResponse Send(IMessage message);
    }
}
