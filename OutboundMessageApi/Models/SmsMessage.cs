﻿using System.Runtime.Serialization;

namespace OutboundMessageApi.Models
{
    [DataContract]
    public class SmsMessage : IMessage
    {
        [DataMember]
        public string ClientId { get; set; }

        [DataMember]
        public string NumberTo { get; set; }

        [DataMember]
        public string NumberFrom { get; set; }

        [DataMember]
        public string MessageBody { get; set; }
    }
}
